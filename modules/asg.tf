/************************************************************************
            Public Auto Scaling Group
*************************************************************************/
//Define the Auto Scaling Group
resource "aws_autoscaling_group" "tf-public-asg" {
  launch_configuration      = "${aws_launch_configuration.terraform-public-lc.id}"
  //Commented out availability_zones because there is a bug in terrform version v0.11.10.
  //If you have availability_zones and vpc_zone_identifier defined you will hit this bug.
  //availability_zones        = ["${var.availability_zone_a}", "${var.availability_zone_b}"]
  name                      = "tf-public-asg"
  vpc_zone_identifier       = ["${aws_subnet.Subnet_A_Public.id}", "${aws_subnet.Subnet_B_Public.id}"]
  min_size                  = "${var.asg_min_pubic}"
  max_size                  = "${var.asg_max_public}"
  desired_capacity          = "${var.asg_desired_capacity_public}"
  health_check_type         = "ELB"

  tags {
    key                     = "Environment"
    value                   = "Development"
    propagate_at_launch     = true
  }
}
/************************************************************************
            END ASG
*************************************************************************/