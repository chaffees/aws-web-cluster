/*********************************************************************
                        Security Group (SG) Definitions
**********************************************************************/

//Define the public security group.
resource "aws_security_group" "tf-public-sg" {
    name    = "TF-Public-SG"
    description = "Allow incoming HTTP/HTTPS connections and SSH access from the Internet."
    vpc_id  = "${aws_vpc.terraform-vpc.id}"

    //Accept tcp port 80 (HTTP) from the Internet.
    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    //Accept tcp port 443 (HTTPS) from the Internet.
    ingress {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    //Accept tcp port 22 (SSH) from the Internet. Modifed port number per SANS security best practice
    ingress {
        from_port   = 4422
        to_port     = 4422
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    //Accept all ICMP inbound from the Internet.
    ingress {
        from_port   = -1
        to_port     = -1
        protocol    = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    //Allow ALL traffic outbound to the Internet.
    egress  {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
        Name    = "Terraform Public SG"
    }
}

//Define the private security group
resource "aws_security_group" "tf-private-sg" {
  name  = "TF-Private-SG"
  description   = "All incoming traffic from the public security group tf-public-sg"
  vpc_id = "${aws_vpc.terraform-vpc.id}"

  //Accept tcp port 3306 (MySQL/AWS Aurora) from the Public SG.
  ingress {
      from_port     = 3306
      to_port       = 3306
      protocol      = "tcp"
      cidr_blocks   = ["${var.public_subnet_a}", "${var.public_subnet_b}"]
  }

  //Accept tcp port 22 (SSH) from the Public SG.
  ingress {
      from_port     = 22
      to_port       = 22
      protocol      = "tcp"
      cidr_blocks   = ["${var.public_subnet_a}", "${var.public_subnet_b}"]
  }

  //Accetp all ICMP inbound from the Public SG.
  ingress {
      from_port     = -1
      to_port       = -1
      protocol      = "icmp"
      cidr_blocks   = ["${var.public_subnet_a}", "${var.public_subnet_b}"]
  }

  //Allow ALL traffic outbound to the Internet.
  egress  {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

  tags {
      Name      = "Terraform Private SG"
  }
}