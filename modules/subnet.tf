/******************************************************************
                        Subnet Definitions
*******************************************************************/

//Define the public subnet for availability zone A.
resource "aws_subnet" "Subnet_A_Public" {
  vpc_id    = "${aws_vpc.terraform-vpc.id}"
  cidr_block    = "${var.public_subnet_a}"
  availability_zone = "${var.availability_zone_a}"

  tags {
      Name  = "Subnet A - Public"
  }
}

//Define the public subnet for availability zone B.
resource "aws_subnet" "Subnet_B_Public" {
  vpc_id    = "${aws_vpc.terraform-vpc.id}"
  cidr_block    = "${var.public_subnet_b}"
  availability_zone = "${var.availability_zone_b}"

  tags {
      Name  = "Subnet B - Public"
  }
}

//Define the private subnet for availability zone A.
resource "aws_subnet" "Subnet_A_Private" {
  vpc_id    = "${aws_vpc.terraform-vpc.id}"
  cidr_block    = "${var.private_subnet_a}"
  availability_zone = "${var.availability_zone_a}"

  tags {
      Name  = "Subnet A - Private"
  }
}

//Define the private subnet for availability zone B.
resource "aws_subnet" "Subnet_B_Private" {
  vpc_id    = "${aws_vpc.terraform-vpc.id}"
  cidr_block    = "${var.private_subnet_b}"
  availability_zone = "${var.availability_zone_b}"

  tags {
      Name  = "Subnet B - Private"
  }
}