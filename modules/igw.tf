/********************************************************************
                        Internet Gateway Definition
*********************************************************************/
resource "aws_internet_gateway" "Terraform-IGW" {
  vpc_id    = "${aws_vpc.terraform-vpc.id}"

  tags {
      Name  = "Terraform-VPC-IGW"
  }
}