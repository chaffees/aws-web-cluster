/********************************************************************
                        Elastic IP Definitions
*********************************************************************/

//Define Elastic IP to be associated with NAT Gateway A.
resource "aws_eip" "Terraform_EIP_A" {
  vpc   = true
  depends_on    = ["aws_internet_gateway.Terraform-IGW"]

  tags {
      Name  = "Terraform EIP A"
  }
}

//Define Elastic IP to be associated with NAT Gateway B.
resource "aws_eip" "Terraform_EIP_B" {
  vpc   = true
  depends_on    = ["aws_internet_gateway.Terraform-IGW"]

  tags {
      Name  = "Terraform EIP B"
  }
}