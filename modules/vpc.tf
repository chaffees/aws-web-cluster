/*******************************************************************
                        Define VPC
********************************************************************/
resource "aws_vpc" "terraform-vpc" {
  cidr_block    = "${var.vpc_cidr}"
  enable_dns_hostnames = true

  tags {
      Name  = "dev-vpc"
  }
}