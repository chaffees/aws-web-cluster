/*********************************************************************
                        NAT Gateway Definitions
**********************************************************************/

//Define the NAT Gateway in the public subnet A for access to the Internet from private subnet A.
resource "aws_nat_gateway" "Terraform_NAT_GW_A" {
  allocation_id = "${aws_eip.Terraform_EIP_A.id}"
  subnet_id = "${aws_subnet.Subnet_A_Public.id}"
  depends_on    = ["aws_internet_gateway.Terraform-IGW"]

  tags {
      Name  = "Terraform NAT GW A"
  }
}

//Define the NAT Gateway in the public subnet B for access to the Internet from private subnet B.
resource "aws_nat_gateway" "Terraform_NAT_GW_B" {
  allocation_id = "${aws_eip.Terraform_EIP_B.id}"
  subnet_id = "${aws_subnet.Subnet_A_Public.id}"
  depends_on    = ["aws_internet_gateway.Terraform-IGW"]

  tags {
      Name  = "Terraform NAT GW B"
  }
}