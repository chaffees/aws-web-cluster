/********************************************************************
                        Route Tables Definitions
*********************************************************************/

//Define public route table for public subnet A.
resource "aws_route_table" "Subnet_A_Route_Table_Public" {
    vpc_id  = "${aws_vpc.terraform-vpc.id}"

    route {
        cidr_block  = "0.0.0.0/0"
        gateway_id  = "${aws_internet_gateway.Terraform-IGW.id}"
    }

    tags {
        Name    = "Subnet A Route Table - Public"
    }
}

//Define public route table for public subnet B.
resource "aws_route_table" "Subnet_B_Route_Table_Public" {
    vpc_id  = "${aws_vpc.terraform-vpc.id}"

    route {
        cidr_block  = "0.0.0.0/0"
        gateway_id  = "${aws_internet_gateway.Terraform-IGW.id}"
    }

    tags {
        Name    = "Subnet B Route Table - Public"
    }
}

//Define the private route table for private subnet A.
resource "aws_route_table" "Subnet_A_Route_Table_Private" {
  vpc_id    = "${aws_vpc.terraform-vpc.id}"

  route {
      cidr_block    = "0.0.0.0/0"
      nat_gateway_id    = "${aws_nat_gateway.Terraform_NAT_GW_A.id}"
  }

  tags {
      Name  = "Subnet A Route Table - Private"
  }
}

//Define the private route table for private subnet B.
resource "aws_route_table" "Subnet_B_Route_Table_Private" {
  vpc_id    = "${aws_vpc.terraform-vpc.id}"

  route {
      cidr_block    = "0.0.0.0/0"
      nat_gateway_id    = "${aws_nat_gateway.Terraform_NAT_GW_B.id}"
  }

  tags {
      Name  = "Subnet B Route Table - Private"
  }
}
/*********************************************************************
                        End Route Table Definitions
**********************************************************************/

/*********************************************************************
                        ROUTE TABLE ASSOCIATIONS
**********************************************************************/

//Associate public subnet A with the public route table A.
resource "aws_route_table_association" "tf-assoc-public-a-rt" {
  subnet_id = "${aws_subnet.Subnet_A_Public.id}"
  route_table_id    = "${aws_route_table.Subnet_A_Route_Table_Public.id}"
}

//Associate public subnet B with the public route table B.
resource "aws_route_table_association" "tf-assoc-public-b-rt" {
  subnet_id = "${aws_subnet.Subnet_B_Public.id}"
  route_table_id    = "${aws_route_table.Subnet_B_Route_Table_Public.id}"
}

//Assocate private subnet A with the private route table A.
resource "aws_route_table_association" "tf-assoc-private-a-rt" {
  subnet_id = "${aws_subnet.Subnet_A_Private.id}"
  route_table_id    = "${aws_route_table.Subnet_A_Route_Table_Private.id}"
}

//Assocate private subnet B with the private route table B.
resource "aws_route_table_association" "tf-assoc-private-b-rt" {
  subnet_id = "${aws_subnet.Subnet_B_Private.id}"
  route_table_id    = "${aws_route_table.Subnet_B_Route_Table_Private.id}"
}
/*********************************************************************
                        END ROUTE TABLE ASSOCIATIONS
**********************************************************************/