variable "vpc_cidr" {
  description   = "CIDR range for the VPC"
  default       = "10.0.0.0/16"
}

variable "environment" {
  description   = "Environment used to deploy infrastructure"
  default       = "Development"
}

variable "public_subnet_a" {
  description   = "CIDR for public subnet"
  default       = "10.0.1.0/24"
}

variable "public_subnet_b" {
  description   = "CIDR for public subnet"
  default       = "10.0.2.0/24"
}
variable "private_subnet_a" {
  description   = "CIDR for the private subnet"
  default       = "10.0.3.0/24"
}

variable "private_subnet_b" {
  description   = "CIDR for the private subnet"
  default       = "10.0.4.0/24"
}
variable "ami" {
  description   = "US-EAST-1 Region Ubuntu Server 18.04 LTS (HVM), SSD Volume Type"
  default       = "ami-0ac019f4fcb7cb7e6"
  //description   = "US-EAST-2 Region Ubuntu Server 18.04 LTS (HVM) - SSD Volume Type"
  //default       = "ami-0f65671a86f061fcd"
}

variable "availability_zone_a" {
  description   = "Availabililty Zone A"
  default       = "us-east-1a"
  //default       = "us-east-2a"
}

variable "availability_zone_b" {
  description   = "Availabililty Zone B"
  default       = "us-east-1b"
  //default       = "us-east-2b"
}

variable "instance_type" {
  description   = "Instance Type"
  default       = "t2.micro"
}

variable "public_instance_name" {
  description   = "Instance Name"
  default       = "tf-web"
}

variable "asg_max_public" {
  description   = "Maximum number of instances public ASG group"
  default       = "10"
}

variable "asg_min_pubic" {
  description   = "Minimum number of instances in public ASG group"
  default       = "2"
}

variable "asg_desired_capacity_public" {
  description   = "The number of Amazon EC2 instances that should be running in the group"
  default       = "2"
}

variable "key_name" {
  description   = "Name of AWS key pair"
  default       = "name_of_key_pair"
}

variable "alb_name" {
  description   = "Name of application load balancer"
  default       = "front-end-alb"  
}

variable "internal_alb" {
  description   = "Value of ALB will determine if it is internal or external. Boolean Value"
  default       = false      
}

variable "alb_idle_timeout" {
  description   = "The time in seconds that the connection is allowed to be idle. Only valid for Load Balancers of type application."
  default       = "300"
}

variable "alb_listener_port" {
  description   = "The port on which the load balancer is listening"
  default       = "80"
}

variable "alb_listener_protocol" {
  description   = "The protocol for connections from clients to the load balancer. Valid values are TCP or HTTP or HTTPS"
  default       = "HTTP"
}
variable "alb_desktop_target_group_name" {
  description   = "The name of the target group."
  default       = "desktop"
}

variable "alb_target_group_port" {
  description   = "The port on which targets receive traffic, unless overridden when registering a specific target"
  default       = "80"
}

variable "alb_target_group_protocol" {
  description   = "The protocol to use for routing traffic to the targets. Should be one of TCP or HTTP or HTTPS or TLS"
  default       = "HTTP"
}

variable "alb_target_group_sticky" {
  description   = "Boolean to enable / disable stickiness. Default is true"
  default       = true
}

variable "alb_static_path" {
  description   = "The path patterns to match. A maximum of 1 can be defined"
  default       = "/var/www/html/*"
}

variable "alb_health_check_path" {
  description   = "The destination for the health check request. Applies to Application Load Balancers only HTTP/HTTPS not Network Load Balancers TCP"
  default       = "/"
}

variable "alb_health_check_port" {
  description   = "The port to use to connect with the target. Valid values are either ports 1-65536"
  default       = "80"
}

variable "fqdn" {
  description   = "Name of domain"
  default       = "*.yourdomain.io"
}

variable "alb_listener_rule_field" {
  description   = "The name of the field. Must be one of path-pattern for path based routing or host-header for host based routing"
  default       = "host-header"
}

variable "alb_listener_rule_values" {
  description   = "The path patterns to match. A maximum of 1 can be defined"
  default       = "helloworld.yourdomain.io"
}

