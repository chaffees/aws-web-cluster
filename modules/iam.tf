resource "aws_iam_role" "ec2_s3_access_role" {
  name  = "tf-s3-FullAccess-Role"
  assume_role_policy  = "${data.aws_iam_policy_document.tf-s3-assume-role-policy.json}"
}
resource "aws_iam_policy" "tf-ec2_s3_access_policy" {
  name  = "tf-s3-ro-policy"
  description = "Read-only access to copy files from s3 to EC2 instances"
  policy  = "${data.aws_iam_policy_document.s3BucketPolicy.json}"
}
resource "aws_iam_policy_attachment" "tf-policy-attachment" {
  name  = "tf-policy-attachment"
  roles  = ["${aws_iam_role.ec2_s3_access_role.name}"]
  policy_arn  = "${aws_iam_policy.tf-ec2_s3_access_policy.arn}"
}
resource "aws_iam_instance_profile" "tf-immutable-profile" {
  name  = "tf-immutable-profile"
  role = "${aws_iam_role.ec2_s3_access_role.name}"
}
data "aws_iam_policy_document" "tf-s3-assume-role-policy" {
  statement {

    actions = ["sts:AssumeRole"]

    principals  {
      type  = "Service"  
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}
data "aws_iam_policy_document" "s3BucketPolicy" {
  statement {

    actions = [
      "s3:ListBucket", 
      "s3:GetObject",
    ]
    resources = [
      "arn:aws:s3:::name-of-S3-bucket", 
      "arn:aws:s3:::name-of-s3-bucket/*",
    ]
    }
}