/**************************************************************************
            PUBLIC Launch Configuration
***************************************************************************/

//Define the Launch Configuration
resource "aws_launch_configuration" "terraform-public-lc" {
  image_id                    = "${var.ami}"
  instance_type               = "${var.instance_type}"
  security_groups             = ["${aws_security_group.tf-public-sg.id}"]
  key_name                    = "${var.key_name}"
  associate_public_ip_address = true
  iam_instance_profile        = "${aws_iam_instance_profile.tf-immutable-profile.name}"

  user_data                   = "${file("cloud-config.yml")}"

  lifecycle {
      create_before_destroy   = true
  }  
}