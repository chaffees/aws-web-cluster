/******************************************************************
                        AWS Certificate Management
 Note: Uncomment the aws_acm_certificate resource if you plan 
 on leveraging AWS ACM to generate and manage certificates.

 Note: Uncomment data: aws_acm_certificate if you are using AWS 
 ACM and AWS Route53 to manage DNS and Certificates.
*******************************************************************/
/*resource "aws_acm_certificate" "Cert" {
    domain_name = "${var.fqdn}"
    validation_method = "DNS"

    tags {
        Environment = "${var.environment}"
    }
}*/

/*data "aws_acm_certificate" "tf-web-cert" {
    domain      = "${var.fqdn}"
    statuses    = ["ISSUED"]
    types       = ["IMPORTED"]
    most_recent = true
}*/
/*******************************************************************
                        End AWS ACM
*******************************************************************/