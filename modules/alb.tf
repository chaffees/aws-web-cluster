/*************************************************************
                        ALB Definition
**************************************************************/
resource "aws_alb" "front_end_alb" {
    name                = "${var.alb_name}"
    internal            = "${var.internal_alb}"
    load_balancer_type  = "application"
    security_groups     = ["${aws_security_group.tf-public-sg.id}"]
    subnets             = ["${aws_subnet.Subnet_A_Public.id}", "${aws_subnet.Subnet_B_Public.id}"]
    idle_timeout        = "${var.alb_idle_timeout}"

    tags {
        Name    = "${var.alb_name}"
    }
}
/*************************************************************
                        End ALB
**************************************************************/

/*************************************************************
                        ALB Target Group
**************************************************************/
resource "aws_alb_target_group" "alb_desktop_target_group" {
  name      = "${var.alb_desktop_target_group_name}"
  port      = "${var.alb_target_group_port}"
  protocol  = "${var.alb_target_group_protocol}"
  vpc_id    = "${aws_vpc.terraform-vpc.id}"

  tags  {
      name  = "${var.alb_desktop_target_group_name}"
  }

  stickiness    {
      type  = "lb_cookie"
      cookie_duration   = 3600
      enabled   = "${var.alb_target_group_sticky}"

  }

  health_check  {
      healthy_threshold = 3
      unhealthy_threshold   = 10
      timeout   = 3
      interval  = 5
      path  = "${var.alb_health_check_path}"
      port  = "${var.alb_health_check_port}"
  }
}
/*************************************************************
                        End ALB Target Group
**************************************************************/

/*************************************************************
                        ALB Listener
 Note: Uncomment certificate_arn if the aws_alb_listener 
 port/protocol are 443/HTTPS and are using AWS ACM 
 and AWS Route53 to manage DNS and Certificates.
**************************************************************/
resource "aws_alb_listener" "desktop_alb_listener" {
  load_balancer_arn = "${aws_alb.front_end_alb.arn}"
  port              = "${var.alb_listener_port}"
  protocol          = "${var.alb_listener_protocol}"

  //certificate_arn   = "${data.aws_acm_certificate.tf-web-cert.arn}"

  default_action {
      type  = "forward"
      target_group_arn  = "${aws_alb_target_group.alb_desktop_target_group.arn}"
  }
}
/*************************************************************
                        End ALB Listener
**************************************************************/

/*************************************************************
                        ALB Listener Rules
**************************************************************/
resource "aws_alb_listener_rule" "static" {
    listener_arn            = "${aws_alb_listener.desktop_alb_listener.arn}"
    
    action  {
        type                = "forward"
        target_group_arn    = "${aws_alb_target_group.alb_desktop_target_group.arn}"
    }

    condition   {
        //field   = "path-pattern"
        //values  = ["${var.alb_static_path}"]
        field = "${var.alb_listener_rule_field}"
        values = ["${var.alb_listener_rule_values}"]
    }
}
/*************************************************************
                        End ALB Listener Rules
**************************************************************/

/*************************************************************
                        ALB Autoscaling Attachment
**************************************************************/
resource "aws_autoscaling_attachment" "alb_target_group_attachment" {
  alb_target_group_arn  = "${aws_alb_target_group.alb_desktop_target_group.arn}"
  autoscaling_group_name    = "${aws_autoscaling_group.tf-public-asg.id}"
}
/*************************************************************
                        End Autoscaling Attachment
**************************************************************/