provider "aws" {
  region  = "${var.aws_region}"
  profile = "dev"
}

module "web-app" {
  source = "../modules"
}