variable "aws_region" {
  description = "Default region for development deployments"

  //Toggle b/w regions
  default = "us-east-1"

  //default     = "us-east-2"
}
