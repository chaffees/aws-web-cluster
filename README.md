# AWS ASG Webserver Deployment with Terraform

This Terraform module creates a configurable [AWS VPC](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html). The network topography was designed to support HA configurations but versatile enough to scale and support fault tolerance environments. The design is intended to be utilized as a framework for [AWS EC2 Auto Scaling](https://docs.aws.amazon.com/autoscaling/plans/userguide/what-is-aws-auto-scaling.html) deployments. The design includes both public and private subnets associated with public and private Security Groups per availability zone. [NAT Gateways](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html) are configured per AZ for use by private subnets. A single [Internet Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html) is created to support routing for public subnets. This module does not include bastion hosts or VPN instance for private subnet access. In addition to the VPC deployment an AWS Application Load Balancer sits in front of the EC2 Auto Scaling Group and scales to all public availability zones defined. The static files will be copied from a configured S3 bucket to each instance in the launch configuration via a cloud-init script. Use this framework as a launchpad to larger more complex environments.

## Example VPC Layout: 2 AZ's

![Example VPC: 2AZ](VPC_w_Public_EC2_ASG.png)

## Getting Started

These instructions will get a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Software and utilities that are required.

```
Terrform >= v0.11.10
aws-cli/1.16.47 Python/3.6.0 Windows/10 botocore/1.12.37
AWS IAM Developer Role
S3 Private Bucket for static web files
```

### Installing

Install Terraform

```
https://www.terraform.io/intro/getting-started/install.html
```

Install AWS CLI

```
Linux  : https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-linux.html
Mac    : https://docs.aws.amazon.com/cli/latest/userguide/cli-install-macos.html
Windows: https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-windows.html
```

AWS Identity Access Management

1.  Create an AWS IAM Development Group
2.  Create AWS IAM User
3.  Add user to group
4.  Assign the following permissions to the group
```
Group Permissions:
AmazonEC2FullAccess
IAMFullAccess
AmazonS3FullAccess
AWSCertificateManagerFullAccess
```
Run the [AWS Configure](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html) command with [Named Profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-multiple-profiles.html)
```
$ aws configure --profile dev
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: us-west-2
Default output format [None]: json
```

Configure S3 Bucket

1. Create S3 Bucket
2. Assign R/W permissions to S3 bucket for the user you created in the IAM steps.

## Deployment

Verify Terraform is installed

```
$ terraform --version
Terraform v0.11.10
```

Verify AWS CLI is installed with a dev profile

```
$ aws --version
aws-cli/1.16.47 Python/3.6.0 Windows/10 botocore/1.12.37

$ cat ~/.aws/config
[profile dev]
region = us-east-2
```

Run Terraform commands:

```
$ terraform init
$ terraform get
$ terraform plan
$ terraform apply
```

## Authors

* **Scott Chaffee** - *Initial work* - [Fuzemedia.io](https://gitlab.com/chaffees)

<!---See also the list of [contributors](https://gitlab.com/chaffees) who participated in this project.--->

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Shoutout to [James Turnbull](https://terraformbook.com/) and his insightfull book!
* [Terraform](https://learn.hashicorp.com/terraform/)
* And to the many forum communities where I asked countless questions on my Terraform journey.

## Additional supporting diagrams

* **Application Load Balancer** - [Diagram](ALB_Diagram.png)
* **Identity Access Management** - [Diagram](AWS_IAM.png)